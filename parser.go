package main

import (
	"encoding/xml"
	"os"
	"time"

	"github.com/araddon/dateparse"
)

type KDRM struct {
	Title string
	Start time.Time
	End   time.Time
}

type KDM struct {
	XMLName             xml.Name `xml:"DCinemaSecurityMessage"`
	AuthenticatedPublic struct {
		RequiredExtensions struct {
			KDMRequiredExtensions struct {
				ContentTitleText          string
				ContentKeysNotValidBefore string
				ContentKeysNotValidAfter  string
			}
		}
	}
}

func parseKDM(fname string) (info KDRM, err error) {
	kdm := KDM{}
	data, err := os.ReadFile(fname)
	if err != nil {
		return
	}

	err = xml.Unmarshal(data, &kdm)
	if err != nil {
		return
	}

	kdrm := kdm.AuthenticatedPublic.RequiredExtensions.KDMRequiredExtensions

	info.Start, err = dateparse.ParseAny(kdrm.ContentKeysNotValidBefore)
	if err != nil {
		return
	}
	info.End, err = dateparse.ParseAny(kdrm.ContentKeysNotValidAfter)
	if err != nil {
		return
	}
	info.Title = kdrm.ContentTitleText
	return
}
