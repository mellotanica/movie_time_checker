package main

import (
	"fmt"
	"log"
	"time"

	arg "github.com/alexflint/go-arg"
	"github.com/araddon/dateparse"
)

var args struct {
	Keyfile   string        `arg:"positional,required" placeholder:"keyfile.xml"`
	Date      string        `arg:"positional" placeholder:"ProjectionDate" help:"format: yyyy-mm-dd"`
	Time      string        `arg:"positional" placeholder:"ProjectionTime" help:"format: hh:mm"`
	Lenght    time.Duration `arg:"positional" placeholder:"ProkectionLength" help:"example: 2h40m"`
	ProjStart time.Time     `arg:"-"`
	ProjEnd   time.Time     `arg:"-"`
	CheckTime bool          `arg:"-"`
	Debug     bool          `arg:"-d"`
}

func pdebug(fmt string, a ...interface{}) {
	if args.Debug {
		log.Printf(fmt, a...)
	}
}

func fmtTime(t time.Time) string {
	return t.In(time.Local).Format("2006-01-02 15:04:05")
}

func main() {
	log.SetFlags(0)
	parser := arg.MustParse(&args)
	var err error

	if args.Date != "" {
		if args.Time == "" {
			parser.Fail("missing ProjectionTime")
		}
		args.ProjStart, err = dateparse.ParseLocal(fmt.Sprintf("%s %s", args.Date, args.Time))
		if err != nil {
			log.Fatal(err)
		}
		args.ProjEnd = args.ProjStart.Add(args.Lenght)
		args.CheckTime = true
	}

	pdebug("input args: %+v", args)

	kdrm, err := parseKDM(args.Keyfile)
	if err != nil {
		log.Fatal(err)
	}

	pdebug("key file: %+v", kdrm)

	if args.CheckTime {
		remainigTime := kdrm.End.Sub(args.ProjStart)

		switch {
		case args.ProjStart.Before(kdrm.Start):
			log.Printf("ERROR: projection starts before key activation (%s)\n", fmtTime(kdrm.Start))
		case args.ProjStart.After(kdrm.End):
			log.Printf("ERROR: projection starts after key expire time (%s)\n", fmtTime(kdrm.End))
		case args.ProjEnd.After(kdrm.End):
			log.Printf("Warning: the key will expire mid-projection (%v left from projection start)\n", remainigTime)
		default:
			log.Printf("OK: it is safe to start projection (%v left from projection start)\n", remainigTime)
		}
	} else {
		log.Printf("Content: %s\nValidity:\n\tfrom: %s\n\tto:   %s\n", kdrm.Title, fmtTime(kdrm.Start), fmtTime(kdrm.End))
	}
}
