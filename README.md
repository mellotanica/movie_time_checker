# movie_time_checker

Simple program that validates and checks KDM movie key files validity period.

## Usage

Key validity reading:

    $> mtc XXX_key_file_YYY.xml
    Content: XXX_YYY_ENG-XX_99_4K_PC_20100321_PDP
    Validity:
        from: 2022-10-08 22:00:00
        to:   2022-10-11 00:59:00

Projection check:

    $> mtc XXX_key_file_YYY.xml 2022-10-10 22:00
    OK, it is safe to start projection (26:59 hours left from prjection start)

Projection check with optional runtime duration:

    $> mtc XXX_key_file_YYY.xml 2022-10-11 23:00 2:40
    WARNING, the key will expire mid-projection (1:59 hours left from prjection start)


