BUILDDIR=build
BINNAME=mtc

all: $(BUILDDIR)/linux/$(BINNAME) $(BUILDDIR)/win/$(BINNAME)

$(BUILDDIR)/linux/$(BINNAME):
	go build -o $@ .

$(BUILDDIR)/win/$(BINNAME):
	GOOS=windows GOARCH=amd64 go build -o $@ .

