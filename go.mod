module gitlab.com/mellotanica/movie_time_checker

go 1.19

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
)

require github.com/alexflint/go-scalar v1.1.0 // indirect
